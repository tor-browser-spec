# General

The audit begins at the commit hash where the previous audit ended. Use code_audit.sh for creating the diff and highlighting potentially problematic code. The audit is scoped to a specific language (currently C/C++, Rust, Java/Kotlin, and Javascript).

The output includes the entire patch where the new problematic code was introduced. Search for `XXX MATCH XXX` to find the next potential violation.

`code_audit.sh` contains the list of known problematic APIs. New usage of these functions are documented and analyzed in this audit.

## Firefox: https://github.com/mozilla/gecko-dev.git

- Start: `$(FIRST_GIT_HASH)` ( `$(START_TAG)` )
- End:   `$(LAST_GIT_HASH)`  ( `$(END_TAG)` )

### Languages:
- [ ] java
- [ ] cpp
- [ ] js
- [ ] rust

Nothing of interest (using `code_audit.sh`)

**OR**

### foreach PROBLEMATIC_HASH:
#### $(PROBLEMATIC_HASH)
- Summary
- Review Result: (SAFE|BAD)

---

## Application Services: https://github.com/mozilla/application-services.git

- Start: `$(FIRST_GIT_HASH)` ( `$(START_TAG)` )
- End:   `$(LAST_GIT_HASH)`  ( `$(END_TAG)` )

### Languages:
- [ ] java
- [ ] cpp
- [ ] js
- [ ] rust

Nothing of interest (using `code_audit.sh`)

**OR**

### foreach PROBLEMATIC_HASH:
#### $(PROBLEMATIC_HASH)
- Summary
- Review Result: (SAFE|BAD)

## Android Components: https://github.com/mozilla-mobile/android-components.git

- Start: `$(FIRST_GIT_HASH)` ( `$(START_TAG)` )
- End:   `$(LAST_GIT_HASH)`  ( `$(END_TAG)` )

### Languages:
- [ ] java
- [ ] cpp
- [ ] js
- [ ] rust

Nothing of interest (using `code_audit.sh`)

**OR**

### foreach PROBLEMATIC_HASH:
#### $(PROBLEMATIC_HASH)
- Summary
- Review Result: (SAFE|BAD)

## Fenix: https://github.com/mozilla-mobile/fenix.git

- Start: `$(FIRST_GIT_HASH)` ( `$(START_TAG)` )
- End:   `$(LAST_GIT_HASH)`  ( `$(END_TAG)` )

### Languages:
- [ ] java
- [ ] cpp
- [ ] js
- [ ] rust

Nothing of interest (using `code_audit.sh`)

**OR**

### foreach PROBLEMATIC_HASH:
#### $(PROBLEMATIC_HASH)
- Summary
- Review Result: (SAFE|BAD)

## Ticket Review ##

Bugzilla Query: `https://bugzilla.mozilla.org/buglist.cgi?query_format=advanced&resolution=FIXED&target_milestone=$(FIREFOX_VERSION)%20Branch&order=priority%2Cbug_severity&limit=0`

where `$(FIREFOX_VERSION)` is the major Firefox version we are auditing (eg: '91')

Nothing of interest (manual inspection)

**OR** (foreach)**

### foreach PROBLEMATIC_TICKET:
#### $(PROBLEMATIC_TICKET)
- Summary
- Review Result: (SAFE|BAD)

## Regression/Prior Vuln Review ##

Review proxy bypass bugs; check for new vectors to look for:
 - https://gitlab.torproject.org/groups/tpo/applications/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Proxy%20Bypass
   - Look for new features like these. Especially external app launch vectors

## Export
- [ ] Export Report and save to `tor-browser-spec/audits`
