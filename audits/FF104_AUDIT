# General

The audit begins at the commit hash where the previous audit ended. Use code_audit.sh for creating the diff and highlighting potentially problematic code. The audit is scoped to a specific language (currently C/C++, Rust, Java/Kotlin, and Javascript).

The output includes the entire patch where the new problematic code was introduced. Search for `XXX MATCH XXX` to find the next potential violation.

`code_audit.sh` contains the list of known problematic APIs. New usage of these functions are documented and analyzed in this audit.

## Firefox: https://github.com/mozilla/gecko-dev.git

- Start: `1f1c56dc6bae6b3302471f097ed132ef44cded86` ( `FIREFOX_103_0_2_RELEASE` )
- End:   `a8c31da1c243a855de8c3b241a437dd1b65684d5`  ( `FIREFOX_104_0_2_RELEASE` )

### Languages:
- [x] java
- [x] cpp
- [x] js
- [x] rust

#### Problematic Commits

- Bug 1780014: Add specific telemetry for conservative and first-try handshakes `7a55bf9c230b83c0a195929feaad1f5e77195412`
  - https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41937
  - **RESOLUTION** nothing to do here, telemetry is gated in the usual fashoin
- Bug 1769994 - [remote] Resolve localhost to an IP before starting httpd.js. `c193147b7b622a9b69e768079553e0d27c05c993`
  - https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41938
  - **RESOLUTION** nothing to do here, this is a debugger feature not a web content one
---

## Application Services: https://github.com/mozilla/application-services.git

- Start: `b70c54882fec606d10e77520b1dd2ae144768747` ( `v94.0.0` )
- End:   `78b165b798118e9b5fa62af07aa44d663f386492`  ( `v94.1.0` )

### Languages:
- [x] java
- [x] cpp
- [x] js
- [x] rust

Nothing of interest (using `code_audit.sh`)

## Android Components: https://github.com/mozilla-mobile/android-components.git

- Start: `ae333f064744e005ef22ac86ab8518d9bf7d9820`
- End:   `7b0499725d3016a0d9288c337fcde4ffff60acfe`  ( `v104.0.10` )

### Languages:
- [x] java
- [x] cpp
- [x] js
- [x] rust

Nothing of interest (using `code_audit.sh`)

## Fenix: https://github.com/mozilla-mobile/fenix.git

- Start: `3f7ddd5d6ef4f8495092138149d36b42e08dbbdb` ( `v104.0b1 )
- End:   `0f9ad767addb7eeef1800ac5bb80e094e3e87f07`  ( `v104.2.0` )

### Languages:
- [x] java
- [x] cpp
- [x] js
- [x] rust

Nothing of interest (using `code_audit.sh`)

## Ticket Review ##

Bugzilla Query: `https://bugzilla.mozilla.org/buglist.cgi?query_format=advanced&resolution=FIXED&target_milestone=104%20Branch&order=priority%2Cbug_severity&limit=0`

#### Problematic Tickets

- **Support fetching data from Remote Setting** https://bugzilla.mozilla.org/show_bug.cgi?id=1728871
  - https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41939
  - **RESOLUTION** nothing to do here, only applies to safe-browsing which we disable
- **When a filetype is set to "always ask" and the user makes a save/open choice in the dialog, we should not also open the downloads panel** https://bugzilla.mozilla.org/show_bug.cgi?id=1739348
  - https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41940
  - **RESOLUTION** was not a security or privacy issue, just needed to make sure it played nciely with our own custom downloads UX which it does
- **Improve Math.pow accuracy for large exponents** https://bugzilla.mozilla.org/show_bug.cgi?id=1775254
  - https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41941
  - **RESOLUTION** tjr fortunately verified this doesn't break existing fingerprinting assumptions upstream

## Export
- [ ] Export Report and save to `tor-browser-spec/audits`